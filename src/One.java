import java.util.Scanner;
public class One {
    public void ONE(){
        Scanner cs = new Scanner(System.in);
        int count = 3;
        Student[] s = new Student[count];
        System.out.println("Заполните студентов");
        for(int i = 0 ; i < count; i++){
            System.out.println("Студент : " + (i+1));
            System.out.print("Фамилия : ");
            String fam =  cs.next();
            System.out.print("Инициалы : ");
            String IN =  cs.next();
            System.out.print("Группа : ");
            String G =  cs.next();
            System.out.println("Введите оценки");
            int[] u = new int[5];
            for(int f=0;f<5;f++){
                u[f] = cs.nextInt();
                if(u[f]>5 || u[f]<0){
                    f--;
                    System.out.println("Оценки не существует");
                }
            }
            s[i] = new Student(i,fam,IN,G,u);
        }
        for(int i = 0 ;i <count-1;i++){
            for(int f =i+1 ; f < count;f++)
                if(s[i].sr>s[f].sr){
                    Student[] st = new Student[1];
                    st[0] = s[i];
                    s[i] = s[f];
                    s[f] = st[0];
                }
        }
        System.out.println("Отсортированые ученики по сред балу");
        for(int i =0 ; i < count;i++){
            System.out.println("Студент : " + s[i].ID);
            System.out.println("Фамилия : " + s[i].fam);
            System.out.println("Инициалы : " + s[i].IN);
            System.out.println("Группа : " + s[i].G);
            System.out.print("Оценки : ");
            for (int j =0 ; j< 5;j++)
                System.out.print(s[i].u[j] + " ");
            System.out.println();
            System.out.println("Средний балл" + s[i].sr);
            System.out.println();
        }
        System.out.println("Студенты имеющие оценки 4 или 5");
        for(int i=0;i<count;i++){
            int a = 0;
            for (int j = 0 ; j<5;j++){
                if(s[i].u[j]>3){
                    a++;
                }
            }
            if(a==5)
                System.out.println("Группа " + s[i].G  + "  Студент " + s[i].fam);
        }
    }
}
