
public class Library {
    int ID;
    String Avtor;
    String name;
    Double price;
    @Override
    public String toString() {
        return String.format("ID : %d\nАвтор : %s\nИмя : %s\nСтоимость : %f\n",ID,Avtor,name,price);
    }
    public Library(int ID,String Avtor,String name,double price){
        this.ID = ID;
        this.Avtor = Avtor;
        this.name = name;
        this.price = price;
    }
}
