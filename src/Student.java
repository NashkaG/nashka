public class Student {
    int ID;
    String fam,IN,G;
    int[] u = new int[5];
    double sr;
    void Sred(int[] u){
        int a = 0;
        for(int i =0 ; i<5;i++){
            a = a + u[i];
        }
        this.sr = a/5;
    }
    @Override
    public String toString() {
        return String.format("ID : %d\nФамилия : %s\nИнициалы : %s\nГруппа : %s\nОценки : %s\nСредний балл : %f\n",ID,fam,G,IN,u.toString(),sr);
    }
    public Student(int ID,String fam,String IN,String G,int[] u){
        Sred(u);
        this.ID = ID;
        this.fam = fam;
        this.IN = IN;
        this.G = G;
        this.u = u;
    }
}
